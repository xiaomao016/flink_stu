package com.zx.flink.window

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.functions.{RichMapFunction, RichReduceFunction}
import org.apache.flink.api.common.state._
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/15 0015 16:35
  * @description:
  *
  */
object StateDemo {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.getConfig.setAutoWatermarkInterval(50)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    val dataStream = env.socketTextStream("192.168.111.12", 7777)


  }
}


class MyRichMap extends RichMapFunction[SensorReading, String] {

  var valueState: ValueState[Double] = _
  lazy val listState: ListState[Double] = getRuntimeContext
    .getListState(new ListStateDescriptor[Double]("listState", classOf[Double]))
  lazy val mapState: MapState[String, Double] = getRuntimeContext
    .getMapState[String, Double](new MapStateDescriptor[String, Double]("listState", classOf[String], classOf[Double]))

  lazy val reduceState: ReducingState[SensorReading] = getRuntimeContext
    .getReducingState(new ReducingStateDescriptor[SensorReading]("mapState", new MyReduceFunc, classOf[SensorReading]))


  override def open(parameters: Configuration): Unit = {
    valueState = getRuntimeContext.getState(new ValueStateDescriptor[Double]("valueState", classOf[Double]))
  }

  override def map(in: SensorReading): String = {
    //测试
    val mvalue = valueState.value()
    valueState.update(12)


    listState.add(1)
    listState.get().forEach(print(_))
    mapState.contains("name")
    mapState.get("NAME")
    mapState.put("name", 123)

    reduceState.add(SensorReading("1", 2, 2))
    reduceState.get()
    in.id

  }
}


class MyReduceFunc extends RichReduceFunction[SensorReading] {
  override def reduce(t: SensorReading, t1: SensorReading): SensorReading = {
    SensorReading(t.id, t1.timestamp, t1.temperature.min(t.temperature))
  }
}