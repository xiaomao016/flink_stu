package com.zx.flink.window

import java.lang

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.assigners._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/9 0009 16:33
  * @description:
  *
  */
object WindowAPIDemo {
  def main(args: Array[String]): Unit = {

    //获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime)

    //获取数据源,转换并分组
    val dataStream = env.socketTextStream("192.168.111.12", 7777)
      .map({
        data => {
          val arr = data.split(",")
          SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
        }
      })
      .map(a => (a.id, a.timestamp, a.temperature))

    val keyStream = dataStream.keyBy(_._1)

    //统计10秒的时间窗口内，温度最小值.这里使用的增量window function：窗口内来一条计算一条
    val windowStreamReduce = keyStream
      .timeWindow(Time.seconds(30))
      .reduce((curData, newData) => {
        (curData._1, newData._2, curData._3.min(newData._3))
      })

    //统计10秒的时间窗口内，温度最小值.这里使用的全窗口函数：窗口结束时全量计算
    val windowStreamAll = keyStream
      .timeWindow(Time.seconds(10))
      .process(new MyProcessFunc)




    //滑动窗口写法:窗口30秒，滑动步长5秒
    val windowStreamSlid = keyStream
      .timeWindow(Time.seconds(30), Time.seconds(5))
      .process(new MyProcessFunc)

    //滚动计数窗口计数:不太常用
    keyStream.countWindow(10)
      .reduce((curData, newData) => {
        (curData._1, newData._2, curData._3.min(newData._3))
      })
    //如果不分区，则使用带All的API
    dataStream.timeWindowAll(Time.seconds(10))
      .reduce((curData, newData) => {
        (curData._1, newData._2, curData._3.min(newData._3))
      })


    //TumblingAlignedProcessingTimeWindows是要废弃的
    //滚动窗口：原生写法.可以加偏移量参数，简写模式不支持，因为参数可能会有歧义。
    keyStream.window(TumblingProcessingTimeWindows.of(Time.seconds(30)))
    keyStream.window(TumblingEventTimeWindows.of(Time.seconds(30)))
    //滑动窗口:步长5
    keyStream.window(SlidingProcessingTimeWindows.of(Time.seconds(30), Time.seconds(5)))
    keyStream.window(SlidingEventTimeWindows.of(Time.seconds(30), Time.seconds(5)))
    //会话时间窗口:简写不支持
    keyStream.window(EventTimeSessionWindows.withGap(Time.minutes(10)))
    keyStream.window(ProcessingTimeSessionWindows.withGap(Time.minutes(10)))


    dataStream.windowAll(TumblingEventTimeWindows.of(Time.seconds(30)))
    dataStream.windowAll(SlidingEventTimeWindows.of(Time.seconds(30),Time.seconds(1)))
//        .apply(new AllWindowFunc001)


    //输出
    //windowStreamReduce.print()

    //    windowStreamAll.print()
    windowStreamSlid.print()

    //执行job
    env.execute("Window API Demo")

  }

}

class MyProcessFunc extends ProcessWindowFunction[(String, Long, Double), (String, Double), String, TimeWindow] {
  override def process(key: String, context: Context, elements: Iterable[(String, Long, Double)], out: Collector[(String, Double)]): Unit = {

    var min = ("", Double.MaxValue)
    for (element <- elements) {
      if (min._2 > element._3) {
        min = (element._1, element._3)
      }
    }
    out.collect(min)
  }
}


class AllWindowFunc001 extends AllWindowFunction[(Long,Long,Double),(String,Double),TimeWindow]{
  override def apply(w: TimeWindow, iterable: lang.Iterable[(Long, Long, Double)], collector: Collector[(String, Double)]): Unit = {

  }
}