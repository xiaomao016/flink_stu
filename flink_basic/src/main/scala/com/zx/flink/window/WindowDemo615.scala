package com.zx.flink.window

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/15 0015 07:44
  * @description:
  *
  *
  *
  */
object WindowDemo615 {

  def main(args: Array[String]): Unit = {
    //获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    //时间语义设置为事件时间时，必须设置assignTimestampsAndWatermarks
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    //获取数据源,转换并分组
    val dataStream = env.socketTextStream("192.168.111.12", 7777)
      .map({
        data => {
          val arr = data.split(",")
          SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
        }
      })
      .map(a => (a.id, a.timestamp, a.temperature))
      .assignAscendingTimestamps(a => a._2 * 1000L)


    val keyStream = dataStream.keyBy(_._1)


    //窗口函数计算:按照事件时间计算10秒内出现的温度最大值以及对应的时间戳和传感器id
    keyStream.window(TumblingEventTimeWindows.of(Time.seconds(10))).reduce((curData, newData) => {
      (curData._1, if (curData._3 > newData._3) newData._2 else curData._2, curData._3.max(newData._3))
    }).print().uid("615")


    //执行Job
    env.execute("window demo")
  }

}
