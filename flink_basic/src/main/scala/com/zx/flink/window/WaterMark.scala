package com.zx.flink.window

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/10 0010 08:07
  * @description:
  *
  */
object WaterMark {

  def main(args: Array[String]): Unit = {
    //获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.getConfig.setAutoWatermarkInterval(50)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    //获取数据源,转换并分组
    val dataStream = env.socketTextStream("192.168.111.12", 7777)
      .map({
        data => {
          val arr = data.split(",")
          SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
        }
      })
      .map(a => (a.id, a.timestamp, a.temperature))

    //要在dataStream上进行设置watermark然后在处理keyedStream
    val keyStream = dataStream
      .assignTimestampsAndWatermarks(
        new BoundedOutOfOrdernessTimestampExtractor[(String, Long, Double)](Time.seconds(3)) {
          override def extractTimestamp(t: (String, Long, Double)): Long = {
            t._2 * 1000L
          }
        }
      ).keyBy(_._1)

    //window->allowLate->sideOutput->窗口函数->侧输出流
    val tag = new OutputTag[(String, Long, Double)]("later")
    val resultStream = keyStream
      .window(TumblingEventTimeWindows.of(Time.seconds(20)))
      .allowedLateness(Time.minutes(1))
      .sideOutputLateData(tag)
      .reduce((curData, newData) => {
        (curData._1, newData._2, curData._3.min(newData._3))
      })

    //输出测流
    resultStream.getSideOutput(tag).print("later")

    resultStream.print("stream")


    //执行job
    env.execute("watermark Demo")

  }

}
