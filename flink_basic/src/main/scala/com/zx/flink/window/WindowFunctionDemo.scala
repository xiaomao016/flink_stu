package com.zx.flink.window

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.functions.{AggregateFunction, ReduceFunction}
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.assigners.{SlidingEventTimeWindows, TumblingAlignedProcessingTimeWindows, TumblingEventTimeWindows}
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/30 0030 13:55
  * @description:
  *
  */
object WindowFunctionDemo {

  def main(args: Array[String]): Unit = {
    //获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    //时间语义设置为事件时间时，必须设置assignTimestampsAndWatermarks
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    //获取数据源,转换并分组
    val dataStream = env.socketTextStream("192.168.111.12", 7777)
      .map({
        data => {
          val arr = data.split(",")
          SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
        }
      })
      .map(a => (a.id, a.timestamp, a.temperature))
      .assignAscendingTimestamps(a => a._2 * 1000L)


    val keyStream = dataStream.keyBy(_._1)

    //1.窗口函数：增量计算
    //1.1.1Reduce函数
    val resultStream = keyStream
      .timeWindow(Time.seconds(30), Time.seconds(5))
      .reduce(new MyReduceFunction)

    //1.1.2 Reduce增量计算，最后将计算结果发给WindowProcessFunction.因为在全量计算函数中可以获取元数据信息，比如窗口结束时间等。
    val resultStream2 = keyStream
      .window(SlidingEventTimeWindows.of(Time.seconds(30), Time.seconds(5)))
      .reduce(new MyReduceFunction, new MyWindowProcessFunction)


    //1.2.1 Aggregate 函数
    val resultStream3 = keyStream
      .window(TumblingEventTimeWindows.of(Time.seconds(30), Time.seconds(5)))
      .aggregate(new MyAggregateFunction)

    //1.2.2 增量计算后，发给windowFunction .注意aggregate函数后的类型要和后一个函数，WindowProcess的参数匹配
    val resultStream4 = keyStream
      .window(SlidingEventTimeWindows.of(Time.seconds(30), Time.seconds(5)))
      .aggregate(new MyAggregateFunction, new MyWindowProcessFunction)


    //2.窗口函数：全量计算
    //2.1.process

    val resultStream5 = keyStream
      .timeWindow(Time.seconds(30), Time.seconds(5))
      .process(new MyWindowProcessFunction)


    env.execute("WindowFunction")

  }

}


case class MaxTempertureInfo(id: String, timestamp: Long, temperature: Double, windowEnd: Long)

//自定义reduce函数
class MyReduceFunction extends ReduceFunction[(String, Long, Double)] {
  override def reduce(t: (String, Long, Double), t1: (String, Long, Double)): (String, Long, Double) = {
    (t._1, t._2.max(t1._2), t._3.min(t1._3))
  }
}

//自定义process函数
class MyWindowProcessFunction extends ProcessWindowFunction[(String, Long, Double), MaxTempertureInfo, String, TimeWindow] {
  override def process(key: String, context: Context,
                       elements: Iterable[(String, Long, Double)],
                       out: Collector[MaxTempertureInfo]): Unit = {
    val e = elements.head
    val r = MaxTempertureInfo(e._1, e._2, e._3, context.window.getEnd)
    out.collect(r)
  }
}

//自定义aggregate函数
class MyAggregateFunction() extends AggregateFunction[(String, Long, Double), (String, Long, Double), (String, Long, Double)] {
  //初始中间状态值
  override def createAccumulator(): (String, Long, Double) = ("", 0L, 0L)

  //新来一个元素，和中间状态计算得到新的状态
  override def add(in: (String, Long, Double), acc: (String, Long, Double)): (String, Long, Double) = {
    (in._1, in._2.max(acc._2), in._3.max(acc._3))
  }

  //获取最终的状态结果
  override def getResult(acc: (String, Long, Double)): (String, Long, Double) = {
    acc
  }

  //合并多个并行任务的状态结果
  override def merge(acc: (String, Long, Double), acc1: (String, Long, Double)): (String, Long, Double) = {
    (acc._1, acc._2.max(acc1._2), acc._3.max(acc1._3))
  }
}

