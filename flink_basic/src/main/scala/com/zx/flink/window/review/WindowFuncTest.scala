package com.zx.flink.window.review

import com.zx.flink.table.review.UserBehavior
import com.zx.flink.window.MaxTempertureInfo
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/9/9 0009 17:27
  * @description:
  *
  */
object WindowFuncTest {

  def main(args: Array[String]): Unit = {
    //1.执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    env.setParallelism(1)


    //2.数据源
    val inputStream = env.readTextFile(getClass.getResource("/UserBehavior").getPath)


    //3.数据转换
    val dataStream = inputStream
      .map(data => {
        val arr = data.split(",")
        UserBehavior(arr(0).toLong, arr(1).toLong, arr(2).toInt, arr(3), arr(4).toLong)
      })
      .filter(_.behavior == "pv")
      .assignAscendingTimestamps(_.timestamp * 1000)


    //4.计算
    val aggStream = dataStream
      .keyBy(_.behavior)
      .window(TumblingEventTimeWindows.of(Time.hours(1)))
      .process(new MyProcessWindowFunction())


    //5.输出
    aggStream.print("")
    //6.执行job
    env.execute("uv job")

  }
}


class MyProcessWindowFunction extends ProcessWindowFunction[UserBehavior,UVCount,String,TimeWindow]{
  override def process(key: String, context: Context, elements: Iterable[UserBehavior], out: Collector[UVCount]): Unit = {

  }
}

//自定义process函数
class MyWindowProcessFunction2 extends ProcessWindowFunction[(String, Long, Double), MaxTempertureInfo, String, TimeWindow] {
  override def process(key: String, context: Context,
                       elements: Iterable[(String, Long, Double)],
                       out: Collector[MaxTempertureInfo]): Unit = {
    val e = elements.head
    val r = MaxTempertureInfo(e._1, e._2, e._3, context.window.getEnd)
    out.collect(r)
  }
}


case class UVCount(count:Long,windowEnd:Long)