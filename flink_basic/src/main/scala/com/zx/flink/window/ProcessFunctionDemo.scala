package com.zx.flink.window

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.state.{ValueState, ValueStateDescriptor}
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.KeyedProcessFunction
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.scala.function.ProcessWindowFunction
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.api.windowing.windows.TimeWindow
import org.apache.flink.util.Collector

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/15 0015 10:24
  * @description:
  *
  */
object ProcessFunctionDemo {

  def main(args: Array[String]): Unit = {

    //获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.getConfig.setAutoWatermarkInterval(500)

    //获取source
    val dataStream = env.socketTextStream("192.168.111.12", 7777)

    //1.1处理转换:ProcessWindowFunction
    val prcfStream = dataStream.map(data => {
      val arr = data.split(",")
      SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
    }).assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.seconds(3)) {
      override def extractTimestamp(t: SensorReading): Long = {
        t.timestamp * 1000L
      }
    }).map(t => (t.id, t.timestamp, t.temperature))
      .keyBy(_._1)
      .timeWindow(Time.seconds(5))
      .process(new MyWindowProcessFunc)


    //1.2.KeyedProcessFunction
    val kprocStream = dataStream.map(data => {
      val arr = data.split(",")
      SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
    }).assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.seconds(3)) {
      override def extractTimestamp(t: SensorReading): Long = {
        t.timestamp * 1000L
      }
    }).keyBy(_.id).process(new TempIncreWarning)


    kprocStream.print()


    //执行job
    env.execute("process function Demo")

  }

}

//计算一个窗口内，如果连续上升，则触发报警
class MyWindowProcessFunc extends ProcessWindowFunction[(String, Long, Double), String, String, TimeWindow] {

  override def process(key: String, context: Context, elements: Iterable[(String, Long, Double)], out: Collector[String]): Unit = {

    val doubles: Iterable[Double] = elements.map(a => a._3)
    var pre = -273D
    var flag = true
    for (d <- doubles if flag) {
      if (pre < 0) {
        pre = d
      } else {
        if (pre > d) {
          flag = false
        }
      }
    }

    if (flag) {
      out.collect("触发报警")
    } else {
      out.collect("温度正常")
    }
  }

}

//计算当10秒内温度连续上升，则触发报警
//1.用状态记录上次温度：用于比较是否温度上升
//2.定时器时间戳：用于删除定时器
//3.当温度第一次出现升高时，设置定时器，如果出现下降，则移除定时器。
//4.定时器触发逻辑中，收集报警信息
class TempIncreWarning extends KeyedProcessFunction[String, SensorReading, String] {
  lazy val lastTempState: ValueState[Double] = getRuntimeContext.getState(new ValueStateDescriptor[Double]("lastTemp", classOf[Double]))
  lazy val timerState: ValueState[Long] = getRuntimeContext.getState(new ValueStateDescriptor[Long]("timerState", classOf[Long]))

  override def processElement(i: SensorReading, context: KeyedProcessFunction[String, SensorReading, String]#Context, collector: Collector[String]): Unit = {

    val lastTemp = lastTempState.value()
    val timerTs = timerState.value()

    //更新温度
    lastTempState.update(i.temperature)

    if (i.temperature > lastTemp && timerTs == 0) {
      //如果温度上升并且没有设置定时器，则设置定时器
      //      val ts = context.timerService().currentWatermark()+10000
      val ts = context.timerService().currentProcessingTime() + 10000
      context.timerService().registerProcessingTimeTimer(ts)
      timerState.update(ts)
    } else if (i.temperature < lastTemp) {
      //如果温度下降，删除定时器
      //      context.timerService().deleteProcessingTimeTimer(timerTs)
      timerState.clear()
    }

  }

  override def onTimer(timestamp: Long, ctx: KeyedProcessFunction[String, SensorReading, String]#OnTimerContext, out: Collector[String]): Unit = {
    out.collect("传感器" + ctx.getCurrentKey + "的温度连续" + 10 + "秒连续上升")
    timerState.clear()
  }
}
