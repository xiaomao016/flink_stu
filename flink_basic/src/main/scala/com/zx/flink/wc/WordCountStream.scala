package com.zx.flink.wc

import org.apache.flink.streaming.api.scala._

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/2 0002 11:16
  * @description:
  *
  */
object WordCountStream {

  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
//        env.setParallelism(8)  //开发环境时，默认并行度就是核心数量，生产环境时配置文件

    val inputDataStream = env.socketTextStream("192.168.111.201", 7777)

    val resultDataStream = inputDataStream
      .flatMap(_.split(" "))
      .filter(_.nonEmpty).disableChaining()
      //      .map((_,1)).setParallelism(2) //可以每一Pa步算子单独设置并行度。一般不设置
      .map((_, 1))
      .keyBy(0)
      .sum(1)
    //    resultDataStream.print().setParallelism(1)  //默认一个并行度，不会打印并行id
    resultDataStream.print().setParallelism(1) //会打印并行job id
    resultDataStream.print("name").setParallelism(2) //会打印并行job id
    inputDataStream.print("aa")
    env.execute("stream wc")
  }


}
