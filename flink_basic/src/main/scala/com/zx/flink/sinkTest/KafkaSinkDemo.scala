package com.zx.flink.sinkTest

import java.util.Properties

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer011, FlinkKafkaProducer011}

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/8 0008 10:11
  * @description:
  *
  */
object KafkaSinkDemo {

  def main(args: Array[String]): Unit = {

    //获取运行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    //获取数据源:kafka

    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "192.168.111.201:9092")
    properties.setProperty("group.id", "consumer-group")

    val kafkaStream = env.addSource(new FlinkKafkaConsumer011[String]("sensor", new SimpleStringSchema(), properties))

    //转换处理

    val ds = kafkaStream.map(
      data => {
        val arr = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble).toString
      }
    )

    //sink输出：kafka_sink
    ds.addSink(new FlinkKafkaProducer011[String]("192.168.111.201:9092", "sink", new SimpleStringSchema()))
    ds.addSink(new FlinkKafkaProducer011[String]("192.168.111.201:9092", "sink2", new SimpleStringSchema()))

    //启动job
    env.execute("kafka sink demo")
  }

}
