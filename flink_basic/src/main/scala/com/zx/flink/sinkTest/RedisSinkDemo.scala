package com.zx.flink.sinkTest

import java.util.Properties

import org.apache.flink.streaming.api.scala._
import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011
import org.apache.flink.streaming.connectors.redis.RedisSink
import org.apache.flink.streaming.connectors.redis.common.config.{FlinkJedisConfigBase, FlinkJedisPoolConfig}
import org.apache.flink.streaming.connectors.redis.common.mapper.{RedisCommand, RedisCommandDescription, RedisMapper}

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/8 0008 14:01
  * @description:
  *
  */
object RedisSinkDemo {

  def main(args: Array[String]): Unit = {

    //获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    //获取数据源:kafka
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "192.168.111.101:9092")
    properties.setProperty("group.id", "consumer-group")

    val kafkaStream = env.addSource(new FlinkKafkaConsumer011[String]("sensor", new SimpleStringSchema(), properties))

    //转换处理
    val ds = kafkaStream.map(
      data => {
        val arr = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
      }
    )

    //sink_redis：写出
    val conf = new FlinkJedisPoolConfig.Builder()
      .setHost("192.168.111.12")
      .setPassword("123456")
      .setPort(6379)
      .build()

    ds.addSink(new RedisSink[SensorReading](conf, new MyRedisMapper))

    //启动job
    env.execute("redis sink Demo")
  }

}

class MyRedisMapper extends RedisMapper[SensorReading] {
  override def getCommandDescription: RedisCommandDescription = {
    new RedisCommandDescription(RedisCommand.HSET, "sensor_temp")
  }

  override def getKeyFromData(t: SensorReading): String = t.id

  override def getValueFromData(t: SensorReading): String = t.temperature.toString
}
