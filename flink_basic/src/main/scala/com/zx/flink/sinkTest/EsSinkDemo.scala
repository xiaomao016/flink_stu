package com.zx.flink.sinkTest

import java.util
import java.util.Properties

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.functions.RuntimeContext
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.elasticsearch.util.RetryRejectedExecutionFailureHandler
import org.apache.flink.streaming.connectors.elasticsearch.{ElasticsearchSinkFunction, RequestIndexer}
import org.apache.flink.streaming.connectors.elasticsearch6.ElasticsearchSink
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011
import org.apache.http.{Header, HttpHost}
import org.apache.http.message.BasicHeader
import org.elasticsearch.client.Requests

object EsSinkDemo {

  def main(args: Array[String]): Unit = {

    //获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    //获取数据源:kafka
    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "192.168.111.101:9092")
    properties.setProperty("group.id", "consumer-group")

    val kafkaStream = env.addSource(new FlinkKafkaConsumer011[String]("sensor", new SimpleStringSchema(), properties))

    kafkaStream.print()
    //转换处理
    val ds = kafkaStream.map(
      data => {
        val arr = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
      }
    )

    //写出：ES_sink
    val httpHosts = new util.ArrayList[HttpHost]()
    httpHosts.add(new HttpHost("dev", 31200))

    val builder = new ElasticsearchSink.Builder[SensorReading](httpHosts, new MyESSinkFunc)

    //要设置批量写入的值，否则一条数据不写入
    builder.setBulkFlushMaxActions(2)
    builder.setRestClientFactory(new RestClientFactoryImpl())
    builder.setFailureHandler(new RetryRejectedExecutionFailureHandler())

    ds.addSink(builder.build())

    //启动job
    env.execute("ES sink Demo")
  }

}

class MyESSinkFunc extends ElasticsearchSinkFunction[SensorReading] {
  override def process(t: SensorReading, runtimeContext: RuntimeContext, requestIndexer: RequestIndexer): Unit = {

    //包装一个map作为dataSource
    val dataSource = new util.HashMap[String, String]()
    dataSource.put("id", t.id)
    dataSource.put("temperature", t.temperature.toString)
    dataSource.put("timestamp", t.timestamp.toString)

    //创建indexReq请求对象
    val indexReq = Requests.indexRequest()
      .index("jjsk_flink")
      .`type`("_doc")
      .source(dataSource)

    //用indexer发送请求
    requestIndexer.add(indexReq)
  }
}

import org.apache.flink.streaming.connectors.elasticsearch6.RestClientFactory
import org.elasticsearch.client.RestClientBuilder

class RestClientFactoryImpl extends RestClientFactory {
  override def configureRestClientBuilder(restClientBuilder: RestClientBuilder): Unit = {
    val headers = new Array[Header](1)
    headers(0) = new BasicHeader("Content-Type", "application/json");
    restClientBuilder.setDefaultHeaders(headers); //以数组的形式可以添加多个header
    restClientBuilder.setMaxRetryTimeoutMillis(90000);
  }
}