package com.zx.flink.streamAPI

import java.util.Properties

import com.alibaba.fastjson.{JSON, JSONObject}
import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011

import scala.util.Random

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/4 0004 09:54
  * @description:
  *
  */
object ExceptionDemo {


  def main(args: Array[String]): Unit = {

    //1.创建执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    val dataList = List(
      "{\"name\":\"zx\"}",
      "{\"name\":\"zx\"}",
      "{\"name\":\"jm\"",
      "{\"name\":\"xiamao\"}"
    )

    //2.数据源
    //来自集合
    val sourceStream = env.fromCollection(dataList)

    //3.转换&输出
    sourceStream
      .map(data => {
        try {
          JSON.parseObject(data)
        } catch {
          case e: Exception => {
            println("json parse error")
          }
        }
      })
      .filter(_.isInstanceOf[JSONObject])
      .map(obj => {
        obj.toString
      })
      .print()

    //4.启动执行job
    env.execute("source test")


  }


}

/**
  * 自定义数据源
  *
  */
class MySource() extends SourceFunction[SensorReading] {

  var running: Boolean = true


  override def cancel(): Unit = running = false

  override def run(sourceContext: SourceFunction.SourceContext[SensorReading]): Unit = {
    //随机数发生器
    var random = new Random()

    //生成数据
    var curTemp = 1.to(10).map(i => ("sensor_" + i, random.nextDouble() * 100))

    while (running) {
      curTemp = curTemp.map(
        data => (data._1, data._2 + random.nextGaussian())
      )

      val curTime = System.currentTimeMillis()
      curTemp.foreach(
        data => sourceContext.collect(SensorReading(data._1, curTime, data._2))
      )
      Thread.sleep(500)
    }
  }
}
