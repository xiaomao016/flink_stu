package com.zx.flink.streamAPI

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.functions.{FilterFunction, MapFunction, ReduceFunction, RichMapFunction}
import org.apache.flink.streaming.api.scala._

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/7 0007 08:39
  * @description:
  *
  */
object TransformDemo {

  def main(args: Array[String]): Unit = {
    //1.获取执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    //2.数据源
    val list = List(3, 6, 9, 2, 4, 100)

    val dataStreamNum = env.fromCollection(list)
    val sensorStream = env.readTextFile("F:\\DM\\BigData\\flink_stu\\flink_basic\\src\\main\\resources\\sensor.txt")
      .map(data => {
        val arr = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
      })


    //3.转换计算
    //map
    val mapStream = dataStreamNum
      .map(new MyMapper)
      .map((_, 1))


    //flatMap,filter
    val flatMapStream = dataStreamNum
      .map(a => a + " " + a * 3)
      .flatMap(a => a.split(" "))
      .map(_.toInt)
      .filter(new MyFilter)


    //keyby
    val keyByStream = dataStreamNum
      .map((_, 1)).setParallelism(1)
      .map(a => (if (a._1 % 2 == 0) 1 else 2, a._1, a._2))
      .keyBy(0)
      .sum(2)


    //滚动聚合
    val rollStream = sensorStream
      .keyBy("id")
      .minBy("temperature")

    val rr = sensorStream
      .keyBy(0)
      .min(2)


    //Reduce算子：这里测试计算得到每个分区温度最小，并且时间戳最近的数据。所以需要温度是按顺序传入，需要设置全局并行度为1
    val reduceStream = sensorStream
      .keyBy("id")
      .reduce(new MyReduce())


    //split和select算子
    val splitStream = sensorStream.split(data => {
      if (data.temperature > 30.0) Seq("high") else Seq("low")
    })

    val highTempStream = splitStream.select("high")
    val lowTempStream = splitStream.select("low")


    //connect 和comap、coflatMap
    val warningStream = highTempStream.map(data => (data.id, data.temperature))
    val connectStream = warningStream.connect(lowTempStream)

    val comapStream = connectStream.map(
      warnData => (warnData._1, warnData._2, "warning"),
      lowData => (lowData.id, "healthy")
    )


    //union
    val unionStream = highTempStream.union(lowTempStream)




    //4.输出
    //    mapStream.print
    //
    //    flatMapStream.print().setParallelism(2)

//        keyByStream.print()
//        rollStream.print().setParallelism(1)
        rr.print().setParallelism(1)

//        reduceStream.print()

    //    highTempStream.print()

//    comapStream.print()
    //5.启动job
    env.execute("transform")


  }

}

//自定义Map函数
class MyMapper extends MapFunction[Int, Int] {

  override def map(t: Int): Int = {
    t * 2
  }
}

class MyMapperRich extends RichMapFunction[Int, Int] {

  override def map(t: Int): Int = {
    t * 2
  }
}

//自定义filter函数
class MyFilter extends FilterFunction[Int] {
  override def filter(t: Int): Boolean = {
    t > 100
  }
}

//自定义Reduce函数:Reduce入参和返回值要保持类型一致
class MyReduce extends ReduceFunction[SensorReading] {
  override def reduce(t: SensorReading, t1: SensorReading): SensorReading = {
    SensorReading(t.id, t1.timestamp, t.temperature.min(t1.temperature))
  }
}