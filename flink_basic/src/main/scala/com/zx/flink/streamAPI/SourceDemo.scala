package com.zx.flink.streamAPI

import java.util.Properties

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer011}

import scala.util.Random

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/4 0004 09:54
  * @description:
  *
  */
object SourceDemo {

  //传感器数据对象：样例类
  case class SensorReading(id: String, timestamp: Long, temperature: Double)

  def main(args: Array[String]): Unit = {

    //1.创建执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val dataList = List(
      SensorReading("sen_1001", 1553232, 35.23),
      SensorReading("sen_1002", 1553223, 36.23),
      SensorReading("sen_1003", 1553244, 33.23),
      SensorReading("sen_1004", 1553221, 34.23)
    )

    //2.数据源
    //来自集合
    val sourceStream = env.fromCollection(dataList)
    //来自文件
    val sourceStream2 = env.readTextFile("F:\\DM\\BigData\\flink_stu\\src\\main\\resources\\hello.txt")

    //来自kafka
    val propertites = new Properties()
    propertites.setProperty("bootstrap.servers", "192.168.111.101:9092")
    propertites.setProperty("group.id", "consumer-group")
    val sourceKafka = env.addSource(new FlinkKafkaConsumer011[String]("sensor", new SimpleStringSchema(), propertites))

    //自定义
    val sourceUD = env.addSource(new MySource2)


    //3.转换&输出
    sourceStream.print()
    sourceStream2.print()
    sourceKafka.print()
    sourceUD.print()
    //4.启动执行job
    env.execute("source test")


  }


}

/**
  * 自定义数据源
  *
  */
class MySource2() extends SourceFunction[SensorReading] {

  var running: Boolean = true


  override def cancel(): Unit = running = false

  override def run(sourceContext: SourceFunction.SourceContext[SensorReading]): Unit = {
    //随机数发生器
    var random = new Random()

    //生成数据
    var curTemp = 1.to(10).map(i => ("sensor_" + i, random.nextDouble() * 100))

    while (running) {
      curTemp = curTemp.map(
        data => (data._1, data._2 + random.nextGaussian())
      )

      val curTime = System.currentTimeMillis()
      curTemp.foreach(
        data => sourceContext.collect(SensorReading(data._1, curTime, data._2))
      )
      Thread.sleep(500)
    }
  }
}
