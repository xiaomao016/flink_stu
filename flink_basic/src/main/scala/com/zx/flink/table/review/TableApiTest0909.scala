package com.zx.flink.table.review

import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala._
import org.apache.flink.table.api.scala._
import org.apache.flink.table.api.{EnvironmentSettings, Table}

object TableApiTest0908 {


  def main(args: Array[String]): Unit = {

    //1.Flink执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    //2.表执行环境:Flink1.12后默认使用BlinkPlanner,本示例Flink版本1.10.1，需要显式声明
    val settings: EnvironmentSettings = EnvironmentSettings
      .newInstance()
      .inStreamingMode()
      .useBlinkPlanner()
      .build()

    val tableEnv: StreamTableEnvironment = StreamTableEnvironment.create(env, settings)

    //3从外部数据源获取数据
    val inputStream = env.socketTextStream("192.168.111.12", 8888)

    //可以先用dataStreamAPI做一些转换处理
    val dataStream: DataStream[UserBehavior] = inputStream
      .map(data => {
        val arr = data.split(",")
        UserBehavior(arr(0).toLong, arr(1).toLong, arr(2).toInt, arr(3), arr(4).toLong)
      })
      .filter(d => {
        d.behavior == "pv"
      })
      .assignAscendingTimestamps(_.timestamp * 1000)

    //4.处理数据
    //4.1.通过Flink SQL处理数据示例
    tableEnv.createTemporaryView("dataTable",dataStream,'userId,'itemId,'behavior,'temperature,'pt.proctime)
//   tableEnv.createTemporaryView("dataTable",table)
    val reTableSQL = tableEnv.sqlQuery(
      """
        |select userId,behavior
        |from dataTable
        |where userId = 10086
      """.stripMargin)

    //5.输出:注意要引入隐式转换
    reTableSQL.toAppendStream[(String, Double)].print("resultTable")

    //6.执行job
    env.execute("Flink SQL Test")


  }

}

/**
  * 用户行为样例类
  * 输入数据格式：543462,1715,1464116,pv,1511658000
  */
case class UserBehavior(userId: Long, itemId: Long, categoryId: Int, behavior: String, timestamp: Long)