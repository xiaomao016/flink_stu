package com.zx.flink.table.review

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.table.api.scala._
import org.apache.flink.table.api.{EnvironmentSettings, Tumble}
import org.apache.flink.types.Row

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/8/30 0030 12:11
  * @description:
  * 如何在tableAPI或SQL中指定时间特性，并提取相应时间。
  * 方式1: dataStream - > Table - >定义
  * 方式2：Connect->在Schema中指定时间语义: 此时需要注意，只有kafka数据源可以识别，其他的数据源要查看是否实现了对应的接口
  * 方式3：DDL中指定，同时指定数据源，这时需要用blink-planner
  *
  * 小结：用dataStream的方式转为Table比较保险。其他方式要必须了解清楚才行。
  */
object TableTimeTest {

  def main(args: Array[String]): Unit = {
    //1.执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    env.setParallelism(1)

    //2.表执行环境
    val settings = EnvironmentSettings
      .newInstance()
      .inStreamingMode()
      .useBlinkPlanner()
      .build()
    val tableEnv = StreamTableEnvironment.create(env,settings)

    //3.数据源
    val inputStream = env.socketTextStream("192.168.111.12", 7777)

    //4.将数据处理：先处理成成样例类，并且设置时间水印
    val dataStream = inputStream
      .map(data=>{
        val arr = data.split(",")
        SensorReading(arr(0),arr(1).toLong,arr(2).toDouble)
      })
      .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.seconds(3)) {
        override def extractTimestamp(t: SensorReading): Long = t.timestamp * 1000
      })


    //============TableAPI-SQL-处理逻辑==============
    //TableAPI
    //1.1.GroupWindow : 每10秒统计一次传感器温度的平均值。
    val sensorTable = tableEnv.fromDataStream(dataStream,'id,'temperature,'timestamp.rowtime as 'ts)
    val resultTable_1_1 = sensorTable
        .window(Tumble over 10.seconds on 'ts as 'tw)
        .groupBy('id,'tw)
        .select('id,'id.count,'temperature.avg,'tw.end)


    val explainTable:String = tableEnv.explain(resultTable_1_1)
    println(explainTable)


    //为了方便观察，每来一条数据，就打印转换后的样例类
    //timestamp - (timestamp - offset + windowSize) % windowSize;
    //窗口起点：[1547718190,1547718200) 考虑watermark会推迟3秒 200的窗口，在203秒才关闭
    dataStream.print("source data")
    //输出统计结果
    resultTable_1_1.toAppendStream[Row].print("tableAPI-Window")



    env.execute("job")











  }

}
