package com.zx.flink.table.review

import java.util

import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala._
import org.apache.flink.table.api.{DataTypes, EnvironmentSettings, Table}
import org.apache.flink.table.api.scala._
import org.apache.flink.table.descriptors.{Csv, FileSystem, Kafka, Schema}

object TableApiTest {


  def main(args: Array[String]): Unit = {

    //1.Flink执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    //2.表执行环境:Flink1.12后默认使用BlinkPlanner,本示例Flink版本1.10.1，需要显式声明
    val settings: EnvironmentSettings = EnvironmentSettings
      .newInstance()
      .inStreamingMode()
      .useBlinkPlanner()
      .build()

    val tableEnv: StreamTableEnvironment = StreamTableEnvironment.create(env, settings)

    //3.连接外部系统，注册表空间
    val prop = new util.Properties()
    prop.setProperty("bootstrap.servers", "192.168.111.101:9092")
    prop.setProperty("zookeeper.connect", "192.168.111.101:2181")

    //从外部数据源获取数据
    tableEnv.connect(new Kafka().version("0.11").topic("sensor").properties(prop))
      .withFormat(new Csv())
      .withSchema(new Schema()
        .field("id", DataTypes.STRING())
        .field("timestamp", DataTypes.BIGINT())
        .field("temperature", DataTypes.DOUBLE())
      )
      .createTemporaryTable("inputTable")


    //4.处理数据
    //4.1.通过TableAPI处理数据示例
    val sensorTable: Table = tableEnv.from("inputTable")

    val resultTable: Table = sensorTable
      .select("id,temperature")
      .filter("id = 'sensor_1' ")

    //4.2.执行SQL处理数据示例
    val resultSQLTable:Table =  tableEnv.sqlQuery(
      """
        |select id,temperature
        |from inputTable where
        |id='sensor_1'
      """.stripMargin)

    //5.输出:注意要引入隐式转换
    resultTable.toAppendStream[(String, Double)].print("resultTable")
    resultSQLTable.toAppendStream[(String,Double)].print("resultSQL")

    //6.执行job
    env.execute("Test")


  }

}
