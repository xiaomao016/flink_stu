package com.zx.flink.table

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.table.api.{DataTypes, TableEnvironment}
import org.apache.flink.table.api.scala._
import org.apache.flink.table.descriptors.{Csv, Kafka, Schema}

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/23 0023 09:00
  * @description:
  *
  */
object TableTimeDemo {

  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    val tableEnv = StreamTableEnvironment.create(env)

    val path = "F:\\DM\\BigData\\flink_stu\\src\\main\\resources\\sensor.txt"
    val dataStream = env.readTextFile(path)

    //1.1.DataStream转换成表时指定指定时间语义
    val table = tableEnv.fromDataStream(dataStream, 'id, 'temperature, 'timestamp, 'pt.proctime)

    //1.2.注册表schema时指定
    tableEnv.connect(
      new Kafka
        version "0.11"
        topic "sensor"
        property("zookeeper.connect", "192.168.111.101:2181")
        property("bootstrap.servers", "192.168.111.101:9092"))
      .withFormat(new Csv)
      .withSchema(new Schema()
        .field("id", DataTypes.STRING())
        .field("timestamp", DataTypes.BIGINT())
        .field("temp", DataTypes.DOUBLE())
        .proctime()
      ).createTemporaryTable("sourceTable")

    //1.3.SQL创建表的DDL中定义
    val sourcetableDDL =
      """
        |create table dataTable(
        | id varchar(20) not null,
        | ts bigint,
        | temperature double,
        | pt AS PROCTIME()
        | ) with(
        | 'connector.type' = 'filesystem',
        | 'connector.path' = '/sensor.txt',
        | 'format.type' = 'csv'
        | )
      """.stripMargin

    tableEnv.sqlUpdate(sourcetableDDL)

    env.execute("TableApiTime Demo")
  }

}
