package com.zx.flink.table

import org.apache.flink.streaming.api.scala._
import org.apache.flink.table.api._
import org.apache.flink.table.api.scala._
import org.apache.flink.table.descriptors.{Csv, FileSystem, Schema}

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/17 0017 08:54
  * @description:
  *
  */
object TableApiDemo01 {


  def main(args: Array[String]): Unit = {

    val env = StreamExecutionEnvironment.getExecutionEnvironment

    //1.创建表执行环境：使用默认配置。默认使用老planer
    val tableEnv = StreamTableEnvironment.create(env)
//
//        //1.1.基于老版本的流处理
//        val DefaultSettings:EnvironmentSettings = EnvironmentSettings
//          .newInstance()
//          .inStreamingMode()
//          .useOldPlanner()
//          .build()
//        val tableEnv01 = StreamTableEnvironment.create(env,DefaultSettings)
//        //1.2.基于老版本的批处理:老版本还没有流批统一，所以需要批处理的环境
//        val batchEnv = ExecutionEnvironment.getExecutionEnvironment
//        val tableEnv02 = BatchTableEnvironment.create(batchEnv)
//
//
//        //1.3.基于新版本的流处理
//        val blinkSettings = EnvironmentSettings
//          .newInstance()
//          .inStreamingMode()
//          .useBlinkPlanner()
//          .build()
//        val tableEnv03 = StreamTableEnvironment.create(env,blinkSettings)
//        //1.4.基于新版本的批处理
//        val blinkBatchSettings = EnvironmentSettings
//          .newInstance()
//          .useBlinkPlanner()
//          .inBatchMode()
//          .build()
//        val blinkBatchTableEnv = TableEnvironment.create(blinkBatchSettings)


    //2.连接外部系统，读取数据
    //2.1.读取文件数据
    val filePath = "F:\\DM\\BigData\\flink_stu\\src\\main\\resources\\sensor.txt"
    tableEnv.connect(new FileSystem().path(filePath))
      .withFormat(new Csv())
      .withSchema(new Schema()
        .field("id", DataTypes.STRING())
        .field("timestamp", DataTypes.BIGINT())
        .field("temperature", DataTypes.DOUBLE())
      )
      .createTemporaryTable("inputTable")


    //3.查询转换
    //3.1.使用Table api
    val sensorTable: Table = tableEnv.from("inputTable")

    //简单转换(使用了symbols表达式)
    val resultTable = sensorTable
      .select('id, 'temperature)
      .filter('id === "sensor_1")


    //3.2.使用SQL
    val resultSQLTable = tableEnv.sqlQuery(
      """
        |select id,temperature
        |form inputTable
        |where id = 'sensor_1'
      """.stripMargin)


    //4.输出
    //4.1.输出到文件系统
    val outFilePath = "F:\\DM\\BigData\\flink_stu\\src\\main\\resources\\sensor.txt"
    tableEnv
      .connect(new FileSystem().path(outFilePath))
      .withFormat(new Csv)
      .withSchema(new Schema()
        .field("id", DataTypes.STRING())
        .field("temperature", DataTypes.DOUBLE())
      )
      .createTemporaryTable("outputTable")

    resultSQLTable.insertInto("outputTable")

    //4.2.打印到控制台
    resultSQLTable.toAppendStream[(String, Double)].print("result")

    env.execute("Table Api Demo01")


  }

}
