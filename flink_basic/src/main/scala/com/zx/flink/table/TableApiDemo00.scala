package com.zx.flink.table

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.streaming.api.scala._
import org.apache.flink.table.api.{EnvironmentSettings, Table}
import org.apache.flink.table.api.scala._

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/17 0017 08:54
  * @description:
  *
  */
object TableApiDemo00 {


  def main(args: Array[String]): Unit = {

    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val resource = getClass.getResource("/sensor.txt")

    val inputStream: DataStream[String] = env.readTextFile(resource.getPath)

    val dataStream = inputStream.map(data => {

      val arr = data.split(",")
      SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
    })

    //1.创建表执行环境：使用默认配置。默认使用老planer
    val settings: EnvironmentSettings = EnvironmentSettings
      .newInstance()
      .inStreamingMode()
      .useBlinkPlanner()
      .build()

    val tableEnv: StreamTableEnvironment = StreamTableEnvironment.create(env, settings)

    //基于流创建表
    val table: Table = tableEnv.fromDataStream(dataStream)


    //调用TableApi进行转换得到结果表
    val reTable: Table = table
      .select("id,temperature")
      .filter("id == 'sensor_1'")

    //注册一个表到catalog,然后查询该表

    tableEnv.createTemporaryView("dataTable", dataStream)
    tableEnv.createTemporaryView("dataTable",table)
    val reTable2 = tableEnv.sqlQuery(
      """
        |select id,temperature
        |from dataTable
        |where id = 'sensor_1'
      """.stripMargin)


    //转换成流输出:这里需要引入隐式转换
    reTable2.toAppendStream[(String, Double)].print("result")


    env.execute("table api demo00")
  }

}
