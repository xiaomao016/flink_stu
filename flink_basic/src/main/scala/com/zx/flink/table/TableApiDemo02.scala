package com.zx.flink.table

import java.util
import java.util.Properties

import org.apache.flink.streaming.api.scala._
import org.apache.flink.table.api.scala._
import org.apache.flink.table.api.{DataTypes, Table, TableEnvironment}
import org.apache.flink.table.descriptors.{Csv, FileSystem, Kafka, Schema}
import org.apache.flink.table.types.DataType
import org.apache.flink.types.Row

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/17 0017 18:13
  * @description:
  *
  * 0.执行步骤，设置，基本概念
  * 1.输出到文件和kafka
  * 2.Table和DataStream转换
  * 3.更新类型
  *
  *
  */
object TableApiDemo02 {

  def main(args: Array[String]): Unit = {

    //1.获取表执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    val tableEnv = StreamTableEnvironment.create(env)

    //2.连接外部系统，注册表空间
    val prop = new util.Properties()
    prop.setProperty("bootstrap.servers", "192.168.111.101:9092")
    prop.setProperty("zookeeper.connect", "192.168.111.101:2181")

    tableEnv
      .connect(new Kafka().version("0.11").topic("sensor").properties(prop))
      .withFormat(new Csv)
      .withSchema(
        new Schema()
          .field("id", DataTypes.STRING())
          .field("timestamp", DataTypes.BIGINT())
          .field("temperature", DataTypes.DOUBLE())
      ).createTemporaryTable("sensorTable")

    //3.获取表对象调用TableAPI或者执行sql
    val table: Table = tableEnv.from("sensorTable")

    val reTable = table
      .select('id, 'temperature)
      .filter('id === "sensor_1")

    val aggreTable = table
      .groupBy('id)
      .select('id, 'id.count as 'cnt)

    //4.写出结果表到外部sink系统

    val path = "F:\\DM\\BigData\\flink_stu\\src\\main\\resources\\out.txt"
    tableEnv.connect(new FileSystem().path(path))
      .inAppendMode()
      .withFormat(new Csv)
      .withSchema(new Schema()
        .field("id", DataTypes.STRING())
        .field("temp", DataTypes.DOUBLE())
      )
      .createTemporaryTable("outputFileTable")


    val propOut = new util.Properties()
    propOut.setProperty("bootstrap.servers", "192.168.111.101:9092")
    propOut.setProperty("zookeeper.connect", "192.168.111.101:2181")
    tableEnv.connect(new Kafka().version("0.11").topic("sink").properties(propOut))
      .withFormat(new Csv)
      .withSchema(
        new Schema()
          .field("id", DataTypes.STRING())
          .field("temp", DataTypes.DOUBLE())
      ).createTemporaryTable("outputKafkaTable")


    reTable.toAppendStream[(String, Double)].print("result")
    reTable.insertInto("outputFileTable")
    reTable.insertInto("outputKafkaTable")

    //转换成撤回流：输出最终都是通过TableSink接口，Kafka实现的类默认是仅追加，ES可以实现Upsert
    aggreTable.toRetractStream[Row].print("aggregate")

    //5.执行job
    env.execute("Table Api Demo02")
  }

}
