package com.zx.flink.table.review

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/8/30 0030 16:52
  * @description:
  *  隐式转换测试
  */
object ImplicitDemo01 {

  def main(args: Array[String]): Unit = {

    implicit def f1(d:Double):Int = {
        d.toInt
    }

    val num:Int = 3.3
    val num2:Int = 3.6
    println(num)
    println(num2)

  }

}
