package com.zx.flink.table

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.table.api.DataTypes
import org.apache.flink.table.api.scala._
import org.apache.flink.table.descriptors.{Csv, Kafka, Rowtime, Schema}

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/23 0023 09:00
  * @description: 事件时间指定
  *
  */
object TableTimeDemo2 {

  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    val tableEnv = StreamTableEnvironment.create(env)

    val path = "F:\\DM\\BigData\\flink_stu\\src\\main\\resources\\sensor.txt"
    val inputStream = env.readTextFile(path)


    val dataStream = inputStream.map(
      data => {
        val arr = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
      }
    ).assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.seconds(1)) {
      override def extractTimestamp(t: SensorReading): Long = t.timestamp * 1000
    })
    //1.1.DataStream转换成表时指定指定事件时间语义，dataStream流已经设置了watermark，这里指定字段名即可
    val table = tableEnv.fromDataStream(dataStream, 'id, 'temperature, 'timestamp, 'rt.rowtime)

    //1.2.注册表schema时指定
    tableEnv.connect(
      new Kafka version "0.11" topic "sensor"
        property("zookeeper.connect", "192.168.111.101:2181")
        property("bootstrap.servers", "192.168.111.101:9092"))
      .withFormat(new Csv)
      .withSchema(new Schema()
        .field("id", DataTypes.STRING())
        .field("timestamp", DataTypes.BIGINT())
        .field("temp", DataTypes.DOUBLE())
        .rowtime(
          new Rowtime()
            .timestampsFromField("timestamp")
            .watermarksPeriodicBounded(1000)
        )
      ).createTemporaryTable("sourceTable")

    //1.3.SQL创建表的DDL中定义
    val sourcetableDDL =
      """
        |create table dataTable(
        | id varchar(20) not null,
        | ts bigint,
        | temperature double,
        | rt AS TO_TIMESTAMP( FROM_UNIXTIME(ts) ),
        | watermark for rt as rt-interval '1' second
        | ) with(
        | 'connector.type' = 'filesystem',
        | 'connector.path' = '/sensor.txt',
        | 'format.type' = 'csv'
        | )
      """.stripMargin

    tableEnv.sqlUpdate(sourcetableDDL)

    env.execute("TableApiTime Demo2")
  }

}
