package com.zx.flink.table

import java.util.Properties

import com.zx.flink.streamAPI.SourceDemo.SensorReading
import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor
import org.apache.flink.streaming.api.scala._
import org.apache.flink.streaming.api.windowing.time.Time
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011
import org.apache.flink.table.api.scala._
import org.apache.flink.table.api._
import org.apache.flink.types.Row

object TableWindowDemo {
  def main(args: Array[String]): Unit = {

    //1.流执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)
    val tableEnv = StreamTableEnvironment.create(env)

    //2.获取source,并注册到表空间
    val props = new Properties
    props.setProperty("bootstrap.servers", "192.168.111.101:9092")
    props.setProperty("group.id", "consumer-group")
    val inputStream = env.addSource(new FlinkKafkaConsumer011[String]("sensor", new SimpleStringSchema(), props))
    val dataStream = inputStream.map(
      data => {
        val arr = data.split(",")
        SensorReading(arr(0), arr(1).toLong, arr(2).toDouble)
      }
    ).assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor[SensorReading](Time.seconds(3)) {
      override def extractTimestamp(t: SensorReading): Long = t.timestamp * 1000
    })

    val sensorTable: Table = tableEnv.fromDataStream(dataStream, 'id, 'timestamp, 'temperature, 'ts.rowtime)

    //3.获取Table对象，操作TableAPI
    //3.1.利用分组窗口函数统计10秒内温度的平均值
    //滚动窗口
    //写法1:table.window(Tumble.over("10").on("ts").as("tw"))
    //写法2:
    val groupWindowResult: Table = sensorTable
      //  .window(Slide over 10.minutes every 5.minutes on 'ts as 'tw) //滑动窗口，滑动步长5分钟
      //  .window(Session.withGap(10.minutes).on('ts).as('tw) ) //session窗口
      .window(Tumble over 50.seconds on 'ts as 'tw)
      .groupBy('id, 'tw)
      .select('id, 'id.count, 'temperature.avg, 'tw.end)


    //3.2.利用over 窗口统计和上次次温度的平均值
    val overWindowResult: Table = sensorTable
      .window(Over partitionBy 'id orderBy 'ts preceding 1.rows as 'ow)
      .select('id, 'ts, 'id.count over 'ow, 'temperature.avg over 'ow)


    //4.sink
    sensorTable.printSchema()
    groupWindowResult.toRetractStream[Row].print()
    groupWindowResult.toAppendStream[Row].print()

    overWindowResult.printSchema()
    overWindowResult.toAppendStream[Row].print()

    //5.执行job
    env.execute("time and window demo")


  }
}
