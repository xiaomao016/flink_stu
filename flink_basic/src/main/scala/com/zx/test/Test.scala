package com.zx.test

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/8/31 0031 15:30
  * @description:
  *
  */
object Test {

  def main(args: Array[String]): Unit = {

    println(2 % 3)
    println(1547718199- 1547718199 % 10)

    println(windowStart(1547718208))

    println((12.3/3).formatted("%.2f"))

  }

  def  windowStart(ti:Long):Long ={
    ti -  (ti % 10)
  }

}
