package com.zx.kafka.producer

import java.util
import java.util.Properties

import org.apache.kafka.clients.producer._
import org.apache.kafka.common.Cluster

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/7/9 0009 09:16
  * @description:
  *
  */
object ProducerDemo {

  def main(args: Array[String]): Unit = {


    val props = new Properties
    //基本参数
    props.setProperty("bootstrap.servers", "nn:9092")
    props.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    //应答机制：0,1,-1. default 1
    props.setProperty("acks", "1")
    //重试次数: 默认2^31 - 1
    props.setProperty("retries", "3")
    //默认是16k,设置单位是字节
    props.setProperty("batch.size", "16384")
    //发送延迟，默认是0
    props.setProperty("linger.ms", "5")


    //自定义分区类:默认是org.apache.kafka.clients.producer.internals.DefaultPartitioner
    props.setProperty("partitioner.class", "com.zx.kafka.producer.MyPartitioner")

    //这里的两个泛型分别表示Key,Value的类型，key用作进行分区时，value就是要发送的数据。
    val producer = new KafkaProducer[String, String](props)

    //1.1.ProducerRecord可以选择发送到哪个分区
    //producer.send(new ProducerRecord[String,String]("first",1,"key.jieM","message01"))

    //1.2.异步发送不带回调
    producer.send(new ProducerRecord[String, String]("first", "message01"))

    //1.3.异步发送并且带回调：此时acks不为0
    producer.send(new ProducerRecord[String, String]("first", "message01"), (recordMetadata: RecordMetadata, e: Exception) => {
      //可以进行逻辑处理，这里可以获取一些元数据信息
      if (e == null) println("success->" + recordMetadata.offset) else e.printStackTrace
    })

    //1.4.同步发送:同步发送的意思就是，一条消息发送之后，会阻塞当前线程，直至返回 ack。
    // 由于 send 方法返回的是一个 Future 对象，根据 Futrue 对象的特点，我们也可以实现同
    //步发送的效果，只需在调用 Future 对象的 get 方发即可。
    producer.send(new ProducerRecord[String, String]("first", "message01")).get()

    //1.5.自定义分区方法:可以利用key进行算法设计.此时要设置props的分区类
    producer.send(new ProducerRecord[String, String]("first", "message01"))


    //关闭生产者
    producer.close

  }

}

/**
  * 自定义分区类
  */
class MyPartitioner extends Partitioner {
  override def partition(s: String, o: Any, bytes: Array[Byte], o1: Any, bytes1: Array[Byte], cluster: Cluster): Int = {
    2
  }

  override def close(): Unit = {

  }

  override def configure(map: util.Map[String, _]): Unit = {

  }
}