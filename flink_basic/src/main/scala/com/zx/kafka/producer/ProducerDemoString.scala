package com.zx.kafka.producer

import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/7/9 0009 09:16
  * @description:
  *
  */
object ProducerDemoString {

  def main(args: Array[String]): Unit = {


    val props = new Properties
    //基本参数
    props.setProperty("bootstrap.servers", "192.168.111.101:9092")
    props.setProperty("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.setProperty("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")

    //应答机制：0,1,-1. default 1
    props.setProperty("acks", "1")
    //重试次数: 默认2^31 - 1
    props.setProperty("retries", "3")
    //默认是16k,设置单位是字节
    props.setProperty("batch.size", "16384")
    //发送延迟，默认是0
    props.setProperty("linger.ms", "5")


    //这里的两个泛型分别表示Key,Value的类型，key用作进行分区时，value就是要发送的数据。
    val producer = new KafkaProducer[String, String](props)


    producer.send(new ProducerRecord[String, String]("first", "message06"))


    //关闭生产者
    producer.close

  }

}
