package com.zx.kafka.Inteceptor

import java.util
import java.util.Properties

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/7/14 0014 09:14
  * @description:
  *
  */
object InteceptorProducer {


  def main(args: Array[String]): Unit = {

    val props = new Properties()
    props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.111.101:9092")
    props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")


    //增加拦截设置
    val list = new util.ArrayList[String]()
    list.add("com.zx.kafka.Inteceptor.TimeInteceptor")
    list.add("com.zx.kafka.Inteceptor.CountInteceptor")

    props.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, list)


    val producer = new KafkaProducer[String, String](props)

    producer.send(new ProducerRecord[String, String]("first", "msg"))

    producer.close()

  }

}
