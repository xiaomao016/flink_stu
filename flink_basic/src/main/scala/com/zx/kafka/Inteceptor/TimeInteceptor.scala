package com.zx.kafka.Inteceptor

import java.util

import org.apache.kafka.clients.producer.{ProducerInterceptor, ProducerRecord, RecordMetadata}

class TimeInteceptor extends ProducerInterceptor[String, String] {
  override def onSend(producerRecord: ProducerRecord[String, String]): ProducerRecord[String, String] = {
    return new ProducerRecord[String, String](producerRecord.topic(), producerRecord.partition(), producerRecord.key(), System.currentTimeMillis() + ":" + producerRecord.value())
  }

  override def onAcknowledgement(recordMetadata: RecordMetadata, e: Exception): Unit = {}

  override def close(): Unit = {}

  override def configure(map: util.Map[String, _]): Unit = {}
}
