package com.zx.kafka.Inteceptor

import java.util

import org.apache.kafka.clients.producer.{ProducerInterceptor, ProducerRecord, RecordMetadata}

class CountInteceptor extends ProducerInterceptor[String, String] {
  private var errorCount: Int = _
  private var successCount: Int = _

  override def onSend(producerRecord: ProducerRecord[String, String]): ProducerRecord[String, String] = {
    producerRecord
  }

  override def onAcknowledgement(recordMetadata: RecordMetadata, e: Exception): Unit = {
    if (e == null) successCount += 1
    else errorCount += 1
  }

  override def close(): Unit = {
    println("successCount:" + successCount + "\n" +
      "errorCount:" + errorCount);
  }

  override def configure(map: util.Map[String, _]): Unit = {}
}
