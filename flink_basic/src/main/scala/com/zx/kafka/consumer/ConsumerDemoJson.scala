package com.zx.kafka.consumer

import java.util
import java.util.Properties

import com.alibaba.fastjson.JSONObject
import org.apache.kafka.clients.consumer.{ConsumerRecord, ConsumerRecords, KafkaConsumer}

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/7/12 0012 15:20
  * @description:
  *
  */
object ConsumerDemoJson {

  def main(args: Array[String]): Unit = {
    //独立一个消费组
    val props = new Properties()
    props.put("bootstrap.servers", "192.168.111.201:9092")
    props.put("group.id", "xm-group")
    props.put("enable.auto.commit", "true")
    props.put("auto.commit.interval.ms", "1000")
    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("value.deserializer", "com.zx.kafka.serializ.JSONDeSerializer")

    val consumer = new KafkaConsumer[String, JSONObject](props)

    consumer.subscribe(util.Arrays.asList("first"))

    while (true) {
      val records: ConsumerRecords[String, JSONObject] = consumer.poll(100)

      val iterator: util.Iterator[ConsumerRecord[String, JSONObject]] = records.iterator()

      //因为采取了json反序列化，record.value()返回的是JSONObject对象。
      //不管发送者是用string序列化还是json序列化，如果采用string反序列化，可以得到json字符串。兼容性最好。
      while (iterator.hasNext) {
        val record = iterator.next()
        printf("offset = %d, key = %s, value %s%n", record.offset(), record.key(), record.value().getString("name"))
      }

    }


  }

}
