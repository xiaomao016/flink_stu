package com.zx.kafka.consumer

import java.util
import java.util.Properties

import com.alibaba.fastjson.JSON
import org.apache.kafka.clients.consumer.{ConsumerRecord, ConsumerRecords, KafkaConsumer}
import org.apache.kafka.common.TopicPartition

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/7/12 0012 15:20
  * @description:
  *
  */
object ConsumerDemoTest {

  def main(args: Array[String]): Unit = {

    val props = new Properties()
    props.put("bootstrap.servers", "192.168.111.101:9092")
    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
    props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")

    //消费者组
    props.put("group.id", "zx-group")
    //设置自动提交offset：提交offset后，该消费者组的offset向前推移。
    props.put("enable.auto.commit", "true")
    //设置自动提交offset时间间隔
    props.put("auto.commit.interval.ms", "1000")


    val consumer = new KafkaConsumer[String, String](props)
//消费指定分区
//    val partition:TopicPartition = new TopicPartition("data.cdc.local",0)
//    consumer.assign(util.Arrays.asList(partition))

    //消费全部分区
    consumer.subscribe(util.Arrays.asList("data.cdc.local"))
    //轮询消息
    while (true) {
      val records: ConsumerRecords[String, String] = consumer.poll(100)

      val iterator: util.Iterator[ConsumerRecord[String, String]] = records.iterator()

      //因为采取了json反序列化，record.value()返回的是JSONObject对象。
      //不管发送者是用string序列化还是json序列化，如果采用string反序列化，可以得到json字符串。兼容性最好。
      while (iterator.hasNext) {
        val record = iterator.next()
        printf("offset = %d, key = %s, value %s%n", record.offset(), record.key(), record.value())
      }

    }


  }

}
