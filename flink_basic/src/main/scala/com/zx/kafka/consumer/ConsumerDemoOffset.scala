package com.zx.kafka.consumer

import java.util
import java.util.Properties

import com.alibaba.fastjson.JSON
import kafka.common.OffsetAndMetadata
import org.apache.kafka.clients.consumer.{ConsumerRecord, ConsumerRecords, KafkaConsumer, OffsetCommitCallback}
import org.apache.kafka.common.TopicPartition

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/7/12 0012 16:14
  * @description:
  *
  */
object ConsumerDemoOffset {

  def main(args: Array[String]): Unit = {
    val props = new Properties()
    //Kafka 集群
    props.put("bootstrap.servers", "192.168.111.101:9092")
    //消费者组，只要 group.id 相同，就属于同一个消费者组
    props.put("group.id", "xm-group")
    props.put("enable.auto.commit", "false") //关闭自动提交 offset
    props.put("key.deserializer",
      "org.apache.kafka.common.serialization.StringDeserializer");
    props.put("value.deserializer",
      "org.apache.kafka.common.serialization.StringDeserializer");

    val consumer: KafkaConsumer[String, String] = new KafkaConsumer(props);
    consumer.subscribe(util.Arrays.asList("first")); //消费者订阅主题
    while (true) {
      //消费者拉取数据
      val records: ConsumerRecords[String, String] = consumer.poll(100);
      val iterator: util.Iterator[ConsumerRecord[String, String]] = records.iterator()

      while (iterator.hasNext) {
        val record = iterator.next()
        printf("offset = %d, key = %s, value %s%n", record.offset(), record.key(), record.value())
      }
      //同步提交，当前线程会阻塞直到 offset 提交成功.同步提交可靠性高，但会阻塞线程
      consumer.commitSync();

      //异步提交，异步提交效率较高。
      consumer.commitAsync()
    }
  }


}