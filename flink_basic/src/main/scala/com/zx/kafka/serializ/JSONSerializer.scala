package com.zx.kafka.serializ

import java.util

import com.alibaba.fastjson.{JSON, JSONObject}
import org.apache.kafka.common.serialization.Serializer

class JSONSerializer extends Serializer[JSONObject] {
  override def configure(map: util.Map[String, _], b: Boolean): Unit = {
  }

  override def serialize(s: String, t: JSONObject): Array[Byte] = {
    JSON.toJSONBytes(t)
  }

  override def close(): Unit = {

  }
}
