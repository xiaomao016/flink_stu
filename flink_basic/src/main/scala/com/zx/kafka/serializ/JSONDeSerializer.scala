package com.zx.kafka.serializ

import java.util

import com.alibaba.fastjson.{JSON, JSONObject}
import org.apache.kafka.common.serialization.Deserializer

class JSONDeSerializer extends Deserializer[JSONObject] {
  override def configure(map: util.Map[String, _], b: Boolean): Unit = {}

  override def deserialize(s: String, bytes: Array[Byte]): JSONObject = JSON.parseObject(bytes, classOf[JSONObject])

  override def close(): Unit = {}
}
