package com.zx.lang.basic

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/9 0009 18:59
  * @description:
  *
  */
object FuncTest {

  def main(args: Array[String]): Unit = {
    //.reduce(
    //      ((s1: (String, Double, Double), s2: (String, Double, Double)) => (s1._1, Math.max(s1._2, s2._2), Math.min(s1._3, s2._3))),
    //      new WindowEndProcessFunction
    //    )

    val list = List(1, 2, 3)

    list.map((_, 1))
      .reduce(
        ((s1: (Int, Int), s2: (Int, Int)) => (s1._1, s1._2 + s2._2))

      )

  }

  def proccess(s: (Int, Int)): (Int, Int) = {
    (0, 0)
  }

}

class MyFunc {
  def proccess(s: (Int, Int)): (Int, Int) = {
    (0, 0)
  }
}
