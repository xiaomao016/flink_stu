package com.zx.lang.basic

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/22 0022 16:07
  * @description:
  *
  */
object TupleDemo {

  def main(args: Array[String]): Unit = {
    val first = (1, 2, 3) // Defining ternary groups

    val one = 1
    val two = 2
    val three = one -> two

    println(three) // Structural binary Group

    println(three._2) // Accessing the second value in a binary tuple

    println("============")
    //Use pattern matching to get members of tuples
    val m = Map(1 -> 2, 2 -> 4)
    for ((k, _) <- m) println(k) //If you don't need all the parts, use ﹐ in this case, only the key is used, so use ﹐ in value_

    println("============")
    //: * as a whole, tell the compiler that you want to treat a parameter as a sequence of numbers
    val s = sum(1 to 5: _*) //Treat 1 to 5 as a sequence
    println(s)

    //<- 遍历
    var list = Array(1, 2, 3, 4)
    for (aa <- list) {
      printf(aa + "   ")
    }
    //symbols使用
    val ss = 'zhangsan
    val tt = 'zhangsan
    println(ss == tt)

  }

  def sum(args: Int*): Int = {
    var result = 0;
    for (s2 <- args) {
      result += s2;
    }
    result;
  }
}
