package com.zx.lang.skill


/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/9/3 0003 08:35
  * @description: List 排序测试
  *
  */
object ListSortOrder {

  def main(args: Array[String]): Unit = {

    //1.数字类的排序测试
    val listNumber = List(12,3,0,6,9)
    val sortedList = listNumber.sorted
    val sortedListDesc = listNumber.sorted(Ordering.Int.reverse)

    println("sorted::"+sortedList)//输出：0,3,6,9,12
    println("sorted-desc::"+sortedListDesc) // 输出：12,9,6,3,0


    //2.根据对象的某个属性进行排序
    val listObject:List[Sensor] = List(
      Sensor("sensor_10",1547718205,38.1),
      Sensor("sensor_10",1547718210,38.3),
      Sensor("sensor_10",1547718215,32.1),
      Sensor("sensor_10",1547718220,48.1)
    )
    //2.1.按照温度值进行排序:sortWith接收一个比较函数，参数是两个要比较的值。比较函数的结果如果为true，则不移动元素，如果为false则往后移动元素
    //所以，下面的表达式，会将小元素移动到后边，所以是一个降序排序，要实现升序排序，只需要将比较函数的>改为小于<即可。
    val listObjSortWith = listObject.sortWith(_.temperature>_.temperature)

    //输出：List(
    // Sensor(sensor_10,1547718220,48.1),
    // Sensor(sensor_10,1547718210,38.3),
    // Sensor(sensor_10,1547718205,38.1),
    // Sensor(sensor_10,1547718215,32.1))
    println("listObjSortWith::"+listObjSortWith)


    //2.2.通过sortBy,并通过函数克里化，传入一个排序表达式，语义上更加清晰:按照温度排序，温度是Double类型，在指明是要降序排序
    val listObjectSortBy = listObject.sortBy(_.temperature)(Ordering.Double.reverse)
    println("listObjectSortBy::"+listObjectSortBy)


    //3.3.自定义排序规则：按照学生数学和英语的平均成绩排序
    val stuList: List[Student] = List(
      Student("1001", 86.5f, 92f),
      Student("1002", 70.5f, 100f),
      Student("1003", 92.5f, 94f),
      Student("1004", 88f, 89f)
    )
    //按照成绩降序排序
    val listStuSortBy = stuList.sortWith(_.compare(_)>0)
    println("listStuSortBy::"+listStuSortBy)



    //4.ListBuffer 排序行为和List一致。
    val listBuffer:ListBuffer[Int] = ListBuffer(12,0,2,4,6)
    val re = listBuffer.sorted
    println("listBuffer::"+re)

  }

}

/**
  * 传感器样例类
  * @param timestamp
  * @param id
  * @param temperature
  */
case class Sensor(id:String,timestamp:Long,temperature:Double)


/**
  * 学生成绩样例类
  * @param id
  * @param math
  * @param english
  */
case class Student(id:String,math:Float,english:Float) extends Ordered[Student]{
  override def compare(that: Student): Int = {
    val avg = (math+english)/2
    val anotherAvg = (that.math+that.english)/2
    if(avg>anotherAvg){
      1
    }else if(avg==anotherAvg){
      0
    }else{
      -1
    }
  }
}
