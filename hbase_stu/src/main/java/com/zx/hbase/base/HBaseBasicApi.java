package com.zx.hbase.base;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2022/1/14 0014 10:57
 * @description:
 */
public class HBaseBasicApi {

    public static void main(String[] args) throws IOException {
        Configuration conf = HBaseConfiguration.create();
        //如果拷贝了hbase-site.xml文件，则不需要设置zk
        //conf.set("hbase.zookeeper.property.clientPort", "2181");
        //conf.set("hbase.zookeeper.quorum", "node-01,node-02,node-03");
        Connection conn = ConnectionFactory.createConnection(conf);

        Admin admin = conn.getAdmin();

        //1.判断命名空间
        try {
            admin.getNamespaceDescriptor("zx");
        } catch (IOException e) {
            admin.createNamespace(NamespaceDescriptor.create("zx").build());
        }

        //2.表操作
        TableName tableName = TableName.valueOf("zx:stu");
        boolean isExist = admin.isTableAvailable(tableName);
        if(!isExist){
            HTableDescriptor tableDes = new HTableDescriptor(tableName);
            HColumnDescriptor columnDes = new HColumnDescriptor("info");
            columnDes.setMaxVersions(5);
            columnDes.setTimeToLive(7*24*60*60);
            tableDes.addFamily(columnDes);
            admin.createTable(tableDes);
        }else{
            //获取表对象
            Table table =conn.getTable(tableName);
            //查询数据
            String rowKey = "1001";

            Get get = new Get(Bytes.toBytes(rowKey));
            Result res = table.get(get);
            if(res.isEmpty()){
                //新增数据
                Put put = new Put(Bytes.toBytes(rowKey));
                put.addColumn(Bytes.toBytes("info"),Bytes.toBytes("name"),Bytes.toBytes("张三"));
//                put.addColumn(Bytes.toBytes("info"),Bytes.toBytes("age"),Bytes.toBytes("13"));
//                put.addColumn(Bytes.toBytes("info"),Bytes.toBytes("phone"),Bytes.toBytes("110"));
                table.put(put);
            }else{
                for(Cell cell:res.rawCells()){
                    System.out.println("row_key={}"+Bytes.toString(CellUtil.cloneRow(cell)));
                    System.out.println("column_family={}"+Bytes.toString(CellUtil.cloneFamily(cell)));
                    System.out.println("column={}"+Bytes.toString(CellUtil.cloneQualifier(cell)));
                    System.out.println("value={}"+Bytes.toString(CellUtil.cloneValue(cell)));
                }
            }
        }


        //3.关闭连接
        admin.close();
        conn.close();









    }
}
