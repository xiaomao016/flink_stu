package com.zx.hbase.base;

import com.zx.hbase.Util.HBaseUtil;

/**
 * Copyright (C) zhongda
 *
 * @author zhangxu
 * @date 2022/1/14 0014 21:39
 * @description:
 */
public class HBaseApi {

    public static void main(String[] args) throws Exception {
        HBaseUtil.connect();
        HBaseUtil.insertData("zx:stu","1002","info","age","33");
        HBaseUtil.close();
    }
}
