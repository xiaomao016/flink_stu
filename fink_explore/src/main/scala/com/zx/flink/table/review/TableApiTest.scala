package com.zx.flink.table.review

import org.apache.flink.streaming.api.TimeCharacteristic
import org.apache.flink.streaming.api.scala._
import org.apache.flink.table.api.bridge.scala.StreamTableEnvironment
import org.apache.flink.table.api.{DataTypes, EnvironmentSettings, Table}
import org.apache.flink.table.api.scala._
import org.apache.flink.table.descriptors.{Csv, FileSystem, Schema}

object TableApiTest {


  def main(args: Array[String]): Unit = {

    //1.执行环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)
    env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime)

    //2.表执行环境
    val settings: EnvironmentSettings = EnvironmentSettings
      .newInstance()
      .inStreamingMode()
      .useBlinkPlanner()
      .build()

    val tableEnv: StreamTableEnvironment = StreamTableEnvironment.create(env, settings)


    //3.从外部数据源获取数据
    val filePath = "F:\\DM\\BigData\\flink_stu\\src\\main\\resources\\sensor.txt"
    tableEnv.connect(new FileSystem().path(filePath))
      .withFormat(new Csv())
      .withSchema(new Schema()
        .field("id", DataTypes.STRING())
        .field("timestamp", DataTypes.BIGINT())
        .field("temperature", DataTypes.DOUBLE())
      )
      .createTemporaryTable("inputTable")


    //4.处理数据

    val sensorTable: Table = tableEnv.from("inputTable")

    val resultTable: Table = sensorTable
      .select("id,temperature")
      .filter("id = 'sensor_1' ")

    //5.输出:注意要引入隐式转换
    resultTable.toAppendStream[(String, Double)].print("result")






  }

}
