package com.zx.flink.wc

import org.apache.flink.api.scala._

/**
  * Copyright (C) zhongda
  *
  * @author zx
  * @date 2021/6/2 0002 10:03
  * @description:
  *
  */
object WordCount {
  def main(args: Array[String]): Unit = {
    //创建一个批处理执行环境
    val env: ExecutionEnvironment = ExecutionEnvironment.getExecutionEnvironment

    //从文件读取批数据
    val inpath = "F:\\DM\\BigData\\flink_stu\\flink_basic\\src\\main\\resources\\hello.txt"
    val inputDataSet = env.readTextFile(inpath)

    val resultDataSet = inputDataSet
      .flatMap(_.split(" "))
      .map((_, 1))
      .groupBy(0)
      .sum(1)

    resultDataSet.print()


  }
}
